# About Deploy

| In terms of | Where we are | Where we want to be |
|-------------|--------------|---------------------|
| Guidelines  | Often engineers (at least in Smarter.Codes) have been trying to 'postpone' deployment until 1-2 months of coding has passed. | We rather want to do it on the [2nd or 3rd day when coding starts](https://gitlab.com/smarter-codes/guidelines/software-engineering/about-software-engineering#option2-architect-review-code-review-deploy-review-repeat). <br><br> On 3rd day of our coding, a project is not ready for [production' deployment but definitely ready for 'Deploy Preview'](https://gitlab.com/smarter-codes/guidelines/software-engineering/deploy/cicd/-/issues/1). |
| Boilerplates| We had some boilerplates coming in @immkg. But not embraced by anyone in Smarter.Codes yet | We can start with a [boilerplate of CICD for every branch](https://gitlab.com/smarter-codes/guidelines/software-engineering/deploy/cicd/-/issues/1). |
| People      | We do not have directory of who has production experience of Kubernetes, CICD, Infrastructure as Code. | [Get them listed in Smarter.Codes Skills Airtable sheet](https://airtable.com/shrdC29iCnExaosLV/tblMlrqiyM9WJxAKv/viwvTOFQqJB4BLxrL). |
| People      | Now that we are [exploding the Full Stack Engineering role](https://gitlab.com/smarter-codes/guidelines/software-engineering/hiring/-/issues/1) we do not know how many Backend Engineers we want for every Full Stack Engineer | Create an issue where this ratio is discussed. So that we recruit enough people |
| Project Management | We are bringing in DevOps team members on ad-hoc basis. Often after 1-2 months of coding time has passed | In every project from Day1 of [Architecture Phase](https://gitlab.com/smarter-codes/guidelines/software-engineering/about-software-engineering#architecture-and-design), We want to announce the team member dedicated to play DevOps/Backend in the team |

# Inspirations
All the [books by Gene Kim](https://www.amazon.com/Gene-Kim/e/B00AERCJ9E/)
